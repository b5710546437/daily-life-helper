package Clock;

/**
 * DISPLAY_TIME is a state that will occur normally.
 * @author Arut Thanomwatana
 *
 */
public class DISPLAY_TIME implements State {

	/**Clock that use to get time.*/
	private Clock clock;
	
	/**
	 * Initialize the state.
	 * @param clock is clock that will be refer to
	 */
	public DISPLAY_TIME(Clock clock) {
		this.clock = clock;
	}

	/**
	 * Perform the method when press plus button.
	 */
	public void performPlus() 
	{
		clock.setState(clock.SHOW_ALARM);
		
	}

	/**
	 * Perform the method when press minus button.
	 */
	public void performMinus()
	{
		clock.setAlarm();
		
	}
	
	/**
	 * Update the time to present.
	 */
	public void updateTime() {
		clock.updateTime();
		
	}

	/**
	 * Perform the method when press set button.
	 */
	public void performSet() {
		clock.setState(clock.SETTING_HOUR);
		
	}
	
	

}
