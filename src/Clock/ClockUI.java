package Clock;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.Observable;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;


public class ClockUI extends JPanel
{		
	javax.swing.Timer timer;
	javax.swing.Timer blink;
	javax.swing.Timer alarm;
	Clock clock; 
	JLabel digit1;
	JLabel digit2;
	JLabel digit3;
	JLabel digit4;
	JLabel digit5;
	JLabel digit6;
	JLabel alarmLabel;
	JLabel set;
	JButton setBtn;
	JButton minusBtn;
	JButton plusBtn;
	int delay;
	boolean visible;

	public ClockUI(Clock clock)
	{
		this.clock = clock;
		visible = true;
		delay = 1000;
		timer = new javax.swing.Timer(delay, task1);
		blink = new javax.swing.Timer(delay/2,task2);
		alarm = new javax.swing.Timer(750,task3);
		this.initComponent();
		timer.start();
		blink.start();
		alarm.setInitialDelay(delay);
		alarm.start();
		this.setVisible(true);
	}

	private void initComponent()
	{
		JPanel bigPanel = new JPanel();
		bigPanel.setLayout(new BoxLayout(bigPanel,BoxLayout.Y_AXIS));

		digit1 = new JLabel();
		digit2 = new JLabel();
		digit3 = new JLabel();
		digit4 = new JLabel();
		digit5 = new JLabel();
		digit6 = new JLabel();

		JPanel panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());
		panel1.setBackground(Color.BLACK);



		JLabel colon1 = new JLabel(":");
		JLabel colon2 = new JLabel(":");
		colon1.setForeground(Color.WHITE);
		colon2.setForeground(Color.WHITE);
		panel1.add(digit1);
		panel1.add(digit2);
		panel1.add(colon1);
		panel1.add(digit3);
		panel1.add(digit4);
		panel1.add(colon2);
		panel1.add(digit5);
		panel1.add(digit6);

		setBtn = new JButton(" SET ");
		setBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				clock.getState().performSet();
			}

		});

		minusBtn = new JButton(" - ");
		minusBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				clock.getState().performMinus();
				if(clock.getAlarm())
					set.setText("ON");
				else
					set.setText("OFF");

			}

		});

		plusBtn = new JButton(" + ");
		plusBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				clock.getState().performPlus();
			}

		});

		alarmLabel = new JLabel("Alarm : ");
		alarmLabel.setForeground(Color.WHITE);
		set = new JLabel();
		set.setForeground(Color.RED);
		if(clock.getAlarm())
			set.setText("ON");
		else
			set.setText("OFF");



		JPanel panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		panel2.setBackground(Color.BLACK);
		panel2.add(setBtn);
		panel2.add(minusBtn);
		panel2.add(plusBtn);

		JPanel panel3 = new JPanel();
		panel3.setLayout(new FlowLayout());
		panel3.setBackground(Color.BLACK);
		panel3.add(alarmLabel);
		panel3.add(set);


		bigPanel.add(panel3);
		bigPanel.add(panel1);
		bigPanel.add(panel2);

		//		panelUp.setBackground(Color.BLACK);
		//		panelDown.setBackground(Color.BLACK);
		//		alarmPanel.add(panelUp);
		//		alarmPanel.add(panelDown);
		//
		//		//ClockUI
		//		//		frame.setBounds(100, 100, 450, 300);
		//		//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//		//		
		//		bgLabel = new JLabel();
		//		bigPanel = new JPanel();
		//		Class cls = this.getClass();
		//		URL url = cls.getResource("../images/clockMain.png");
		//		ImageIcon imgIcon2 = new ImageIcon(url);
		//		bgLabel.setIcon(imgIcon2);
		//		bgLabel.setBounds(0, 0, 800, 600);
		//
		//		alarmPanel.setBounds(135, 300, 200, 140);
		//
		//		bigPanel.add(bgLabel);
		//		super.add(alarmPanel);
		//		super.add(bigPanel);

		super.add(bigPanel);
		
	}

	public void run()
	{
		
	}

	public ImageIcon[] getImage()
	{
		ImageIcon [] imageSet = new ImageIcon[11];  
		ClassLoader loader = this.getClass().getClassLoader();
		URL imageScan; 
		ImageIcon img;

		for(int i =0;i<10;i++)
		{	
			String index = "images/"+i+".gif";
			imageScan = loader.getResource(index);
			img = new ImageIcon(imageScan);
			imageSet[i] = img;
		}
		imageScan = loader.getResource("images/blank.gif");
		img = new ImageIcon(imageScan);
		imageSet[10] = img; 
		return imageSet;
	}

	public void playAlarm(){
		try {
			Clip clip = AudioSystem.getClip();
			AudioInputStream alarm = AudioSystem.getAudioInputStream(Main.class.getResourceAsStream("sound/alarm.wav"));
			clip.open(alarm);
			clip.start();

		} catch (LineUnavailableException e) {
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	ActionListener task1 = new ActionListener(){
		public void actionPerformed(ActionEvent evt){
			ImageIcon [] imageSet = getImage();
			clock.getState().updateTime();
			if(clock.getState()==clock.DISPLAY_TIME){
				visible = true;
				digit1.setIcon(imageSet[clock.getHour()/10]);
				digit2.setIcon(imageSet[clock.getHour()%10]);
				digit3.setIcon(imageSet[clock.getMin()/10]);
				digit4.setIcon(imageSet[clock.getMin()%10]);
				digit5.setIcon(imageSet[clock.getSec()/10]);
				digit6.setIcon(imageSet[clock.getSec()%10]);
			}
			else if(clock.getState()==clock.SHOW_ALARM||clock.getState()==clock.SETTING_HOUR)
			{
				digit1.setIcon(imageSet[clock.getAlarmHour()/10]);
				digit2.setIcon(imageSet[clock.getAlarmHour()%10]);
				digit3.setIcon(imageSet[clock.getAlarmMin()/10]);
				digit4.setIcon(imageSet[clock.getAlarmMin()%10]);
				digit5.setIcon(imageSet[clock.getAlarmSec()/10]);
				digit6.setIcon(imageSet[clock.getAlarmSec()%10]);
			}




		}
	};

	ActionListener task3 = new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			ImageIcon [] imageSet = getImage();
			if((clock.checkAlarm()||clock.getState()==clock.ALARM_RINGING)&&clock.getAlarm())
			{
				playAlarm();
				if(visible){
					digit1.setIcon(imageSet[10]);
					digit2.setIcon(imageSet[10]);
					digit3.setIcon(imageSet[10]);
					digit4.setIcon(imageSet[10]);
					digit5.setIcon(imageSet[10]);
					digit6.setIcon(imageSet[10]);
					visible = false;

				}
				else{
					visible = true;
					digit1.setIcon(imageSet[clock.getHour()/10]);
					digit2.setIcon(imageSet[clock.getHour()%10]);
					digit3.setIcon(imageSet[clock.getMin()/10]);
					digit4.setIcon(imageSet[clock.getMin()%10]);
					digit5.setIcon(imageSet[clock.getSec()/10]);
					digit6.setIcon(imageSet[clock.getSec()%10]);
				}
				clock.setState(clock.ALARM_RINGING);
			}

		}
	};

	ActionListener task2 = new ActionListener(){

		public void actionPerformed(ActionEvent arg0) {
			ImageIcon [] imageSet = getImage();
			if(clock.getState()==clock.SETTING_HOUR){
				if(visible){
					visible = false;
					digit1.setIcon(imageSet[10]);
					digit2.setIcon(imageSet[10]);
				}
				else
				{
					visible = true;
					digit1.setIcon(imageSet[clock.getAlarmHour()/10]);
					digit2.setIcon(imageSet[clock.getAlarmHour()%10]);
				}
			}

			else if(clock.getState()==clock.SETTING_MIN){
				digit1.setIcon(imageSet[clock.getAlarmHour()/10]);
				digit2.setIcon(imageSet[clock.getAlarmHour()%10]);
				if(visible){
					visible = false;
					digit3.setIcon(imageSet[10]);
					digit4.setIcon(imageSet[10]);
				}
				else
				{
					visible = true;
					digit3.setIcon(imageSet[clock.getAlarmMin()/10]);
					digit4.setIcon(imageSet[clock.getAlarmMin()%10]);
				}
			}

			else if(clock.getState()==clock.SETTING_SEC)
			{
				digit3.setIcon(imageSet[clock.getAlarmMin()/10]);
				digit4.setIcon(imageSet[clock.getAlarmMin()%10]);
				if(visible){
					visible =false;
					digit5.setIcon(imageSet[10]);
					digit6.setIcon(imageSet[10]);
				}
				else
				{
					visible = true;
					digit5.setIcon(imageSet[clock.getAlarmSec()/10]);
					digit6.setIcon(imageSet[clock.getAlarmSec()%10]);
				}
			}
		}
	};





	public ImageIcon[] getImageIcon() {
		Class cls = this.getClass();
		ImageIcon[] imgIcon = new ImageIcon[10];
		for (int i = 0; i < 10; i++) {
			URL digit = cls.getResource("../images/" + i + ".png");
			ImageIcon icon = new ImageIcon(digit);
			imgIcon[i] = icon;

		}
		return imgIcon;

	}


}

