package Clock;

/**
 * SETTING_HOUR is a state that will occur when we want to set hour for an alarm.
 * @author Arut Thanomwatana
 *
 */
public class SETTING_HOUR implements State{
	
	/**Clock that use to get time.*/
	private Clock clock;
	
	/**
	 * Initialize the state.
	 * @param clock is clock that will be refer to
	 */
	public SETTING_HOUR(Clock clock) {
		this.clock = clock;
	}

	/**
	 * Perform the method when press plus button.
	 */
	public void performPlus() {
		clock.setAlarmHour(clock.getAlarmHour()+1);
	}

	/**
	 * Update the time to present.
	 */
	public void updateTime() {
		
	}

	/**
	 * Perform the method when press set button.
	 */
	public void performSet() {
		clock.setState(clock.SETTING_MIN);
		
	}
	
	/**
	 * Perform the method when press minus button.
	 */
	public void performMinus()
	{
		clock.setAlarmHour(clock.getAlarmHour()-1);
		
	}
	
}
