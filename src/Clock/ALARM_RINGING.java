package Clock;
/**
 * ALARM_RINGING is a state that will occur when the alarm go off
 * @author Arut Thanomwatana
 *
 */
public class ALARM_RINGING implements State {

	/**Clock that use to get time.*/
	private Clock clock;
	
	/**
	 * Initialize the state.
	 * @param clock is clock that will be refer to
	 */
	public ALARM_RINGING(Clock clock) {
		this.clock = clock;
	}

	/**
	 * Update the time to present.
	 */
	public void updateTime() {
		clock.updateTime();
	}

	/**
	 * Perform the method when press set button.
	 */
	public void performSet() {
		clock.setState(clock.DISPLAY_TIME);

	}

	/**
	 * Perform the method when press plus button.
	 */
	public void performPlus() {
	
	}

	/**
	 * Perform the method when press minus button.
	 */
	public void performMinus() {
		
	}

}
