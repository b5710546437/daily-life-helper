package Clock;

/**
 * SETTING_HOUR is a state that will occur when we want to set minute for an alarm.
 * @author Arut Thanomwatana
 *
 */
public class SETTING_MIN implements State {

	/**Clock is use to get the time*/
	private Clock clock;
	/**
	 * Initialize the state
	 * @param clock is the clock that will refer to.
	 */
	public SETTING_MIN(Clock clock) {
		this.clock = clock;
	}

	/**
	 * Perform the method when press plus button.
	 */
	public void performPlus() {
		clock.setAlarmMin(clock.getAlarmMin()+1);;

	}

	/**
	 * Update the time to present.
	 */
	public void updateTime() {

	}

	/**
	 * Perform the method when press set button.
	 */
	public void performSet() {
		clock.setState(clock.SETTING_SEC);

	}
	
	/**
	 * Perform the method when press minus button.
	 */
	public void performMinus(){
		clock.setAlarmMin(clock.getAlarmMin()-1);
	}

}
