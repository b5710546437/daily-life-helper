package Clock;

/**
 * An interface for every state in clock.
 * @author Arut Thanomwatana
 *
 */
public interface State 
{
	/**Perform the method when press plus button.*/
	public void performPlus();
	
	/**Update time to present. */
	public void updateTime();
	
	/**Perform the method when press set button. */
	public void performSet();
	
	/**Perform the method when press minus button.*/
	public void performMinus();

}
