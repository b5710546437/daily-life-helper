package Clock;

/**
 * Run the CheapClock.
 * @author Arut Thanomwatana
 *
 */
public class Main 
{
	public static void main(String[]args){
		Clock clock = new Clock();
		ClockUI cheap = new ClockUI(clock);
		cheap.run();
		
	}

}
