package Clock;
import java.util.Calendar;
import java.util.Observable;

/**
 * Clock is the class that will handle every method in CheapClock.
 * @author Arut Thanomwatana
 *
 */
public class Clock 
{
	/**Hour of the day.*/
	private int hour;
	/**Minute of that hour.*/
	private int min;
	/**Second of that minute.*/
	private int sec;
	/**Hour that was set to alarm.*/
	private int alarmHour;
	/**Minute of that hour that was set to alarm.*/
	private int alarmMin;
	/**Second of that minute that was set to alarm.*/
	private int alarmSec;
	/**Current state of clock.*/
	private State state;
	/**ON/OFF alarm.*/
	private boolean alarm;
	
	/**Display time state.*/
	State DISPLAY_TIME = new DISPLAY_TIME(this);
	/**Alarm ringing state.*/
	State ALARM_RINGING = new ALARM_RINGING(this);
	/**Set hour alarm state.*/
	State SETTING_HOUR = new SETTING_HOUR(this);
	/**Set minute alarm state.*/
	State SETTING_MIN = new SETTING_MIN(this);
	/**Set second alarm state.*/
	State SETTING_SEC = new SETTING_SEC(this);
	/**Show alarm state.*/
	State SHOW_ALARM = new SHOW_ALARM(this);
	

	/**
	 * Initialize the clock.
	 */
	public Clock()
	{
		this.state = DISPLAY_TIME;
		this.alarmHour = 0;
		this.alarmMin = 0;
		this.alarmSec = 0;
		alarm = false;
	}

	/**
	 * Update time to present.
	 */
	public void updateTime()
	{
		Calendar c = Calendar.getInstance();
		hour = c.get(c.HOUR_OF_DAY);
		min = c.get(c.MINUTE);
		sec = c.get(c.SECOND);
	
	}

	/**
	 * Get hour.
	 * @return hour of the day
	 */
	public int getHour()
	{
		return this.hour;
	}
	
	/**
	 * Get minute.
	 * @return minute of the hour
	 */
	public int getMin()
	{
		return this.min;
	}

	/**
	 * Get second.
	 * @return second of the minute.
	 */
	public int getSec()
	{
		return this.sec;
	}
	
	/**
	 * Get alarm Hour.
	 * @return alarm hour that was set.
	 */
	public int getAlarmHour()
	{
		return this.alarmHour;
	}

	/**
	 * Get alarm minute.
	 * @return alarm minute that was set.
	 */
	public int getAlarmMin()
	{
		return this.alarmMin;
	}

	/**
	 * Get alarm second.
	 * @return alarm second that was set.
	 */
	public int getAlarmSec()
	{
		return this.alarmSec;
	}
	
	/**
	 * set the alarm hour.
	 * @param hour is the hour that will be set
	 */
	public void setAlarmHour(int hour)
	{
		if(hour>23)
			hour = 0;
		else if(hour<0)
			hour = 23;
		this.alarmHour=hour;
	}
	
	/**
	 * set the alarm minute.
	 * @param min is the minute that will be set
	 */
	public void setAlarmMin(int min){
		if(min>59)
			min =0;
		else if(min<0)
			min = 59;
		this.alarmMin = min;
	}
	
	/**
	 * set the alarm second.
	 * @param sec is the second that will be set.
	 */
	public void setAlarmSec(int sec){
		if(sec > 59)
			sec = 0;
		else if(sec<0)
			sec = 59;
		this.alarmSec = sec;
	}

	/**
	 * set the state
	 * @param state is the state that will be set.
	 */
	public void setState(State state)
	{
		this.state = state;
	}
	
	/**
	 * get the state
	 * @return current state.
	 */
	public State getState()
	{
		return this.state;
	}

	/**
	 * check whether the current time will ring the alarm or not.
	 * @return true if current time will ring the alarm false if not
	 */
	public boolean checkAlarm(){
		return this.hour==this.alarmHour&&this.min==this.alarmMin&&this.sec==this.alarmSec;
	}
	
	/**
	 * set the ON/OOF of the alarm
	 */
	public void setAlarm()
	{
		if(this.alarm){
			this.alarm = false;
			return;
		}
		else
			this.alarm = true;
	}
	
	/**
	 * Get the alarm switch. 
	 * @return alarm wheter it is on or not
	 */
	public boolean getAlarm(){
		return this.alarm;
	}
	
	
	
}

