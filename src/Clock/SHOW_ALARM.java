package Clock;

/**
 * SHOW_ALARM is a state that will occur when we want to see the alarm that was set.
 * @author Arut Thanomwatana
 *
 */
public class SHOW_ALARM implements State
{
	/**Clock is use to get the time*/
	private Clock clock;

	/**
	 * Initialize the state
	 * @param clock is the clock that will refer to.
	 */
	public SHOW_ALARM(Clock clock) {
		this.clock = clock;
	}

	/**
	 * Perform the method when press plus button.
	 */
	public void performPlus() {
		clock.setState(clock.DISPLAY_TIME);
		
	}
	
	/**
	 * Perform the method when press minus button.
	 */
	public void performMinus(){
		
	}

	/**
	 * Update the time to present.
	 */
	public void updateTime() {
		
	}

	/**
	 * Perform the method when press set button.
	 */
	public void performSet() {
		
	}

}
