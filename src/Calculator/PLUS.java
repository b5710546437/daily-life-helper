package Calculator;

/**
 * A plus state
 * @author Arut Thanomwattana, Narut Poovorakit
 *
 */
public class PLUS implements State 
{
	private Calculator cal;
	
	/**
	 * A plus constructor
	 * @param cal is a calculator.
	 */
	public PLUS(Calculator cal){
		this.cal = cal;
	}
	
	/**
	 * A plus state and set a new state and calculate a number.
	 */
	@Override
	public void performPlus(double num) {
		this.cal.setState(cal.PLUS);
		this.cal.setLastState(cal.PLUS);
		this.cal.setNumber1(cal.getNumber1() + num);
	}

	/**
	 * A minus state and set a new state and calculate a number.
	 */
	@Override
	public void performMinus(double num) {
		this.cal.setState(cal.MINUS);
		this.cal.setLastState(cal.PLUS);
		this.cal.setNumber1(cal.getNumber1() + num);
	}

	/**
	 * A multiply state and set a new state and calculate a number.
	 */
	@Override
	public void performMultiply(double num) {
		this.cal.setState(cal.MULTIPLY);
		this.cal.setLastState(cal.PLUS);
		this.cal.setNumber1(cal.getNumber1() + num);
	}

	/**
	 * A divide state and set a new state and calculate a number.
	 */
	@Override
	public void performDivide(double num) {
		this.cal.setState(cal.DIVIDE);
		this.cal.setLastState(cal.PLUS);
		this.cal.setNumber1(cal.getNumber1() + num);
	}

	/**
	 * A resut state and set a new state and calculate a number.
	 */
	@Override
	public void performResult(double num) {
		cal.setState(cal.RESULT);
		cal.setLastState(cal.PLUS);
		cal.setNumber2(num);
		cal.performOperation();
	}

	/**
	 * A mod state and set a new state and calculate a number.
	 */
	@Override
	public void performMod(double num) {
		this.cal.setState(cal.MOD);
		this.cal.setLastState(cal.PLUS);
		this.cal.setNumber1(cal.getNumber1() + num);
	}
}
