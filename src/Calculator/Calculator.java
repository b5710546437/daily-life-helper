package Calculator;

/**
 * A calculator class
 * @author Arut thanomwattana, Narut Poovorakit
 *
 */
public class Calculator {
	
	private double num1;
	
	private double num2;
	
	private double result;
	
	private State state;
	private State lastState;
	
	State NORMAL = new NORMAL(this);
	State PLUS = new PLUS(this);
	State MINUS = new MINUS(this);
	State DIVIDE = new DIVIDE(this);
	State MULTIPLY = new MULTIPLY(this);
	State RESULT = new RESULT(this);
	State MOD = new MOD(this);
	
	/**
	 * A calculator constructor
	 */
	public Calculator(){
		this.num1 = 0;
		this.num2 = 0;
		result = 0;
		this.state = NORMAL;
		this.lastState = NORMAL;
	}

	/**
	 * Calculate a number of each operation.
	 */
	public void performOperation(){
		
		if(this.lastState == this.PLUS){
			System.out.println(num2);
			this.num1 += this.num2;
		}
		else if(this.lastState == this.MINUS){
			this.num1 -= this.num2;
		}
		else if(this.lastState == this.DIVIDE){
			this.num1 /= this.num2;
		}
		else if(this.lastState == this.MULTIPLY){
			this.num1 *= this.num2;
		}
		else if(this.lastState == this.RESULT){
			result = this.num1;
		}
	}
	
	/**
	 * Set a number
	 * @param num1 is a number that bring to set.
	 */
	public void setNumber1(double num1) {
		this.num1 = num1;
	}
	
	/**
	 * Set a number
	 * @param num2 is a number that bring to set.
	 */
	public void setNumber2(double num2) {
		this.num2 = num2;
	}
	
	/**
	 * 
	 * @return a number
	 */
	public double getNumber1() {
		return num1;
	}
	
	/**
	 * 
	 * @return a number
	 */
	public double getNumber2(){
		return num2;
	}
	
	/**
	 * Clear all of the text.
	 */
	public void clear(){
		this.num1 = 0;
		this.num2 = 0;
		this.result = 0;
		this.state = NORMAL;
		this.lastState = NORMAL;
		//this.oper = "";
	}
	
	/**
	 * Set a new state.
	 * @param newState is a new state that bring to set.
	 */
	public void setState(State newState){
		this.state = newState;
	}
	
	/**
	 * Get a state
	 * @return a state
	 */
	public State getState(){
		return this.state;
	}
	
	/**
	 * Get last state
	 * @return a last state.
	 */
	public State getLastState() {
		return this.lastState;
	}
	
	/**
	 * Set a last state.
	 * @param lastState is a state.
	 */
	public void setLastState(State lastState) {
		this.lastState = lastState;
	}
}
