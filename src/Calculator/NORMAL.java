package Calculator;

/**
 * A normal state or the first state.
 * @author Arut thanomwattana, Narut poovorakit
 *
 */
public class NORMAL implements State
{

	private Calculator cal;
	
	/**
	 * A normal constructor
	 * @param cal is a calculator
	 */
	public NORMAL(Calculator cal){
		this.cal = cal;
	}
	
	/**
	 * A plus state and set a new state.
	 */
	@Override
	public void performPlus(double num) {
		this.cal.setState(cal.PLUS);
		this.cal.setLastState(cal.NORMAL);
		this.cal.setNumber1(num);
	}

	/**
	 * A minus state and set a new state.
	 */
	@Override
	public void performMinus(double num) {
		this.cal.setState(cal.MINUS);
		this.cal.setLastState(cal.NORMAL);
		this.cal.setNumber1(num);
	}

	/**
	 * A multiply state and set a new state.
	 */
	@Override
	public void performMultiply(double num) {
		this.cal.setState(cal.MULTIPLY);
		this.cal.setLastState(cal.NORMAL);
		this.cal.setNumber1(num);
	}

	/**
	 * A divide state and set a new state.
	 */
	@Override
	public void performDivide(double num) {
		this.cal.setState(cal.DIVIDE);
		this.cal.setLastState(cal.NORMAL);
		this.cal.setNumber1(num);
	}

	/**
	 * A result state and set a new state.
	 */
	@Override
	public void performResult(double num) {
		this.cal.setState(cal.RESULT);
		this.cal.performOperation();
	}

	/**
	 * A mod state and set a new state.
	 */
	@Override
	public void performMod(double num) {
		this.cal.setState(cal.MOD);
		this.cal.setLastState(cal.NORMAL);
		this.cal.setNumber1(num);
	}
}
