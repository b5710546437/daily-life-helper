package Calculator;

import java.awt.Color;
import java.awt.EventQueue;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Font;

/**
 * A ui of calculator.
 * 
 * @author Arut Thanomwattana, Narut Poovorakit
 *
 */
public class CalculatorUI extends JFrame {

	private JFrame frame;
	private JPanel pane;
	private JLabel calculatorPic, bg, saveNum;
	private JButton plusBut, minusBut, divideBut, multiplyBut, piBut, clearBut,
			oneBut, twoBut, threeBut, fourBut, fiveBut, sixBut, sevenBut,
			eightBut, nineBut, zeroBut, dotBut, modBut, ansBut, exitBut;
	private JTextField numText;
	private double add, savenum;
	private boolean check = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalculatorUI window = new CalculatorUI();
					// window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CalculatorUI() {
		super.setUndecorated(true);
		super.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(200, 100, 350, 550);
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	/**
	 * 
	 */
	private void initialize() {
		ClassLoader cls = this.getClass().getClassLoader();
		Calculator cal = new Calculator();

		frame = new JFrame();
		frame.setBounds(100, 100, 600, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Label

		URL url2 = cls.getResource("images/calBackground.png");
		ImageIcon bgIcon = new ImageIcon(url2);
		bg = new JLabel();
		bg.setIcon(bgIcon);
		bg.setBounds(0, 0, 350, 550);

		// Text
		numText = new JTextField();
		numText.setBounds(-1, 36, 351, 53);

		// button
		URL urlexit = cls.getResource("images/exitBut.png");
		ImageIcon exitIcon = new ImageIcon(urlexit);
		exitBut = new JButton();
		exitBut.setIcon(exitIcon);
		exitBut.setBounds(320, 0, 30, 30);
		exitBut.setContentAreaFilled(false);
		exitBut.addActionListener(p -> {
			super.setVisible(false);
		});

		URL url3 = cls.getResource("images/Button/plus.png");
		ImageIcon plusIcon = new ImageIcon(url3);
		plusBut = new JButton();
		plusBut.setIcon(plusIcon);
		plusBut.setContentAreaFilled(false);
		plusBut.setBounds(94, 129, 93, 40);

		plusBut.addActionListener(p -> {
			if (numText.getText().equals(""))
				cal.getState().performPlus(0);
			else {
				double num = Double.parseDouble(numText.getText());
				cal.getState().performPlus(num);
			}
			numText.setText("");
		});

		URL url4 = cls.getResource("images/Button/minus.png");
		ImageIcon minusIcon = new ImageIcon(url4);
		minusBut = new JButton();
		minusBut.setIcon(minusIcon);
		minusBut.setContentAreaFilled(false);
		minusBut.setBounds(0, 129, 93, 40);
		minusBut.addActionListener(p -> {
			if (numText.getText().equals(""))
				cal.getState().performMinus(0);
			else {
				double num = Double.parseDouble(numText.getText());
				cal.getState().performMinus(
						Double.parseDouble(numText.getText()));
			}
			numText.setText("");
		});

		URL url5 = cls.getResource("images/Button/divide.png");
		ImageIcon divideIcon = new ImageIcon(url5);
		divideBut = new JButton("/");
		divideBut.setIcon(divideIcon);
		divideBut.setContentAreaFilled(false);
		divideBut.setBounds(94, 89, 93, 40);
		divideBut.addActionListener(p -> {
			if (numText.getText().equals(""))
				cal.getState().performDivide(0);
			else {
				double num = Double.parseDouble(numText.getText());
				cal.getState().performDivide(
						Double.parseDouble(numText.getText()));
			}
			numText.setText("");
		});

		URL url6 = cls.getResource("images/Button/multiply.png");
		ImageIcon multiplyIcon = new ImageIcon(url6);
		multiplyBut = new JButton("*");
		multiplyBut.setContentAreaFilled(false);
		multiplyBut.setIcon(multiplyIcon);
		multiplyBut.setBounds(-1, 89, 94, 40);
		multiplyBut.addActionListener(p -> {
			if (numText.getText().equals(""))
				cal.getState().performMultiply(0);
			else {
				double num = Double.parseDouble(numText.getText());
				cal.getState().performMultiply(
						Double.parseDouble(numText.getText()));
			}
			numText.setText("");
		});

		URL url7 = cls.getResource("images/Button/mod.png");
		ImageIcon modIcon = new ImageIcon(url7);
		modBut = new JButton("Mod");
		modBut.setIcon(modIcon);
		modBut.setContentAreaFilled(false);
		modBut.setBounds(188, 89, 94, 40);
		modBut.addActionListener(p -> {
			if (numText.getText().equals(""))
				cal.getState().performMod(0);
			else {
				double num = Double.parseDouble(numText.getText());
				cal.getState()
						.performMod(Double.parseDouble(numText.getText()));
			}
			numText.setText("");
		});

		URL url8 = cls.getResource("images/Button/pi.png");

		ImageIcon piIcon = new ImageIcon(url8);
		piBut = new JButton("PI");
		piBut.setIcon(piIcon);
		piBut.setContentAreaFilled(false);
		piBut.setBounds(188, 129, 94, 40);
		piBut.addActionListener(p -> {
			numText.setText(Double.toString(Math.PI));
		});

		clearBut = new JButton("Clear");
		clearBut.setFont(new Font("Tahoma", Font.PLAIN, 10));
		clearBut.setBounds(287, 89, 63, 77);
		clearBut.addActionListener(p -> {
			numText.setText("");
			cal.clear();
		});

		URL url9 = cls.getResource("images/Button/1.png");
		ImageIcon oneIcon = new ImageIcon(url9);
		oneBut = new JButton("1");
		oneBut.setIcon(oneIcon);
		oneBut.setContentAreaFilled(false);
		oneBut.setBounds(-1, 381, 116, 100);
		oneBut.addActionListener(p -> {
			if (check) {
				numText.setText("1");
				check = false;
			} else {
				String text = numText.getText();
				numText.setText(text + "1");
			}
		});

		URL url10 = cls.getResource("images/Button/2.png");
		ImageIcon twoIcon = new ImageIcon(url10);
		twoBut = new JButton("2");
		twoBut.setIcon(twoIcon);
		twoBut.setContentAreaFilled(false);
		twoBut.setBounds(117, 381, 116, 100);
		twoBut.addActionListener(p -> {
			if (check) {
				numText.setText("2");
				check = false;
			} else {
				String text = numText.getText();
				numText.setText(text + "2");
			}
		});

		URL url11 = cls.getResource("images/Button/3.png");
		ImageIcon threeIcon = new ImageIcon(url11);
		threeBut = new JButton("3");
		threeBut.setIcon(threeIcon);
		threeBut.setContentAreaFilled(false);
		threeBut.setBounds(234, 381, 116, 100);
		threeBut.addActionListener(p -> {
			if (check) {
				numText.setText("3");
				check = false;
			} else {
				String text = numText.getText();
				numText.setText(text + "3");
			}
		});

		URL url12 = cls.getResource("images/Button/4.png");
		ImageIcon fourIcon = new ImageIcon(url12);
		fourBut = new JButton("4");
		fourBut.setIcon(fourIcon);
		fourBut.setContentAreaFilled(false);
		fourBut.setBounds(0, 270, 116, 100);
		fourBut.addActionListener(p -> {
			if (check) {
				numText.setText("4");
				check = false;
			} else {
				String text = numText.getText();
				numText.setText(text + "4");
			}
		});

		URL url13 = cls.getResource("images/Button/5.png");
		ImageIcon fiveIcon = new ImageIcon(url13);
		fiveBut = new JButton("5");
		fiveBut.setIcon(fiveIcon);
		fiveBut.setContentAreaFilled(false);
		fiveBut.setBounds(117, 270, 116, 100);
		fiveBut.addActionListener(p -> {
			if (check) {
				numText.setText("5");
				check = false;
			} else {
				String text = numText.getText();
				numText.setText(text + "5");
			}
		});

		URL url14 = cls.getResource("images/Button/6.png");
		ImageIcon sixIcon = new ImageIcon(url14);
		sixBut = new JButton("6");
		sixBut.setIcon(sixIcon);
		sixBut.setContentAreaFilled(false);
		sixBut.setBounds(234, 270, 116, 100);
		sixBut.addActionListener(p -> {
			if (check) {
				numText.setText("6");
				check = false;
			} else {
				String text = numText.getText();
				numText.setText(text + "6");
			}
		});

		URL url15 = cls.getResource("images/Button/7.png");
		ImageIcon sevenIcon = new ImageIcon(url15);
		sevenBut = new JButton("7");
		sevenBut.setIcon(sevenIcon);
		sevenBut.setContentAreaFilled(false);
		sevenBut.setBounds(-1, 169, 116, 100);
		sevenBut.addActionListener(p -> {
			if (check) {
				numText.setText("7");
				check = false;
			} else {
				String text = numText.getText();
				numText.setText(text + "7");
			}
		});

		URL url16 = cls.getResource("images/Button/8.png");
		ImageIcon eightIcon = new ImageIcon(url16);
		eightBut = new JButton("8");
		eightBut.setIcon(eightIcon);
		eightBut.setContentAreaFilled(false);
		eightBut.setBounds(117, 169, 116, 100);
		eightBut.addActionListener(p -> {
			if (check) {
				numText.setText("8");
				check = false;
			} else {
				String text = numText.getText();
				numText.setText(text + "8");
			}
		});

		URL url17 = cls.getResource("images/Button/9.png");
		ImageIcon nineIcon = new ImageIcon(url17);
		nineBut = new JButton("9");
		nineBut.setIcon(nineIcon);
		nineBut.setContentAreaFilled(false);
		nineBut.setBounds(234, 169, 116, 100);
		nineBut.addActionListener(p -> {
			if (check) {
				numText.setText("9");
				check = false;
			} else {
				String text = numText.getText();
				numText.setText(text + "9");
			}
		});

		URL url18 = cls.getResource("images/Button/0.png");
		ImageIcon zeroIcon = new ImageIcon(url18);
		zeroBut = new JButton("0");
		zeroBut.setIcon(zeroIcon);
		zeroBut.setContentAreaFilled(false);
		zeroBut.setBounds(0, 487, 116, 63);
		zeroBut.addActionListener(p -> {
			if (check) {
				numText.setText("0");
				check = false;
			} else {
				String text = numText.getText();
				numText.setText(text + "0");
			}
		});

		URL url19 = cls.getResource("images/Button/dot.png");
		ImageIcon dotIcon = new ImageIcon(url19);
		dotBut = new JButton(".");
		dotBut.setIcon(dotIcon);
		dotBut.setContentAreaFilled(false);
		dotBut.setBounds(127, 487, 93, 63);
		dotBut.addActionListener(p -> {
			String text = numText.getText();
			numText.setText(text + ".");
		});

		URL url20 = cls.getResource("images/Button/equal.png");
		ImageIcon ansIcon = new ImageIcon(url20);
		ansBut = new JButton(" = ");
		ansBut.setIcon(ansIcon);
		ansBut.setContentAreaFilled(false);
		ansBut.setBounds(230, 487, 116, 63);
		ansBut.addActionListener(p -> {
			if (numText.getText().equals("")) {
				double num = cal.getNumber1();
				cal.getState().performResult(num);
			} else {
				cal.getState().performResult(
						Double.parseDouble(numText.getText()));
			}
			numText.setText(Double.toString(cal.getNumber1()));
			System.out.println(cal.getNumber1());
		});

		// Panel
		pane = new JPanel();
		pane.setLayout(null);
		pane.add(exitBut);
		pane.add(ansBut);
		pane.add(numText);
		pane.add(plusBut);
		pane.add(minusBut);
		pane.add(divideBut);
		pane.add(multiplyBut);
		pane.add(piBut);
		pane.add(clearBut);
		pane.add(oneBut);
		pane.add(twoBut);
		pane.add(threeBut);
		pane.add(fourBut);
		pane.add(fiveBut);
		pane.add(sixBut);
		pane.add(sevenBut);
		pane.add(eightBut);
		pane.add(nineBut);
		pane.add(zeroBut);
		pane.add(dotBut);
		pane.add(modBut);
		pane.add(bg);
		pane.setBackground(Color.BLACK);
		super.add(pane);

	}
	
	/**
	 * Run the UI
	 */
	public void run(){
		super.setVisible(true);
	}

}
