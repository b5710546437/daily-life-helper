package Calculator;

/**
 * A divide state
 * @author Arut Thanomwattana, Narut Poovorakit
 *
 */
public class DIVIDE implements State{

	private Calculator cal;
	
	/**
	 * A divide constructor
	 * @param cal is a calculator
	 */
	public DIVIDE(Calculator cal) {
		this.cal = cal;
	}
	
	/**
	 * perform a plus state and set a last state.
	 */
	@Override
	public void performPlus(double num) {
		this.cal.setState(cal.PLUS);
		this.cal.setLastState(cal.DIVIDE);
		this.cal.setNumber1(cal.getNumber1() / num);
	}

	/**
	 * perform a minus state and set a last state.
	 */
	@Override
	public void performMinus(double num) {
		this.cal.setState(cal.MINUS);
		this.cal.setLastState(cal.DIVIDE);
		this.cal.setNumber1(cal.getNumber1() / num);
	}

	/**
	 * perform a multiply state and set a last state.
	 */
	@Override
	public void performMultiply(double num) {
		this.cal.setState(cal.MULTIPLY);
		this.cal.setState(cal.DIVIDE);
		this.cal.setNumber1(cal.getNumber1() / num);
	}

	/**
	 * perform a divide state and set a last state.
	 */
	@Override
	public void performDivide(double num) {
		this.cal.setState(cal.DIVIDE);
		this.cal.setLastState(cal.DIVIDE);
		this.cal.setNumber1(cal.getNumber1() / num);
	}

	/**
	 * perform a result state and set a last state.
	 */
	@Override
	public void performResult(double num) {
		this.cal.setState(cal.RESULT);
		this.cal.setLastState(cal.DIVIDE);
		this.cal.setNumber2(num);
		this.cal.performOperation();
	}

	/**
	 * perform a mod state and set a last state.
	 */
	@Override
	public void performMod(double num) {
		this.cal.setState(cal.MOD);
		this.cal.setLastState(cal.DIVIDE);
		this.cal.setNumber1(cal.getNumber1() / num);
	}
}
