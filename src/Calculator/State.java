package Calculator;

/**
 * A interface of a state
 * @author Arut thanomwattana, Narut Poovorakit
 *
 */
public interface State 
{
	public void performPlus(double num);
	
	public void performMinus(double num);
	
	public void performMultiply(double num);
	
	public void performDivide(double num);
	
	public void performMod(double num);
	
	public void performResult(double num);
	
}
