package Calculator;

/**
 * A result state.
 * @author Arut thanomwattana, Narut Poovorakit
 *
 */
public class RESULT implements State {

	private Calculator cal;
	
	/**
	 * A result constructor
	 * @param cal is a calculator
	 */
	public RESULT(Calculator cal) {
		this.cal = cal;
	}
	
	/**
	 * A plus state and set a new state.
	 */
	@Override
	public void performPlus(double num) {
		this.cal.setState(cal.PLUS);
	}

	/**
	 * A minus state and set a new state.
	 */
	@Override
	public void performMinus(double num) {
		this.cal.setState(cal.MINUS);
	}

	/**
	 * A multiply state and set a new state.
	 */
	@Override
	public void performMultiply(double num) {
		this.cal.setState(cal.MULTIPLY);
	}

	/**
	 * A divide state and set a new state.
	 */
	@Override
	public void performDivide(double num) {
		this.cal.setState(cal.DIVIDE);
	}

	/**
	 * A result state and set a new state.
	 */
	@Override
	public void performResult(double num) {
		this.cal.setState(cal.RESULT);
		this.cal.performOperation();
	}

	/**
	 * A mod state and set a new state.
	 */
	@Override
	public void performMod(double num) {
		this.cal.setState(cal.MOD);
	}
}
