package Calculator;

/**
 * A mod state
 * @author Arut Thanomwattana, Narut Poovorakit
 *
 */
public class MOD implements State{

	private Calculator cal;
	
	public MOD(Calculator cal){
		this.cal = cal;
	}
	
	/**
	 * A plus state and set a new state and calculate a number.
	 */
	@Override
	public void performPlus(double num) {
		this.cal.setState(cal.PLUS);
		this.cal.setLastState(cal.MOD);
		this.cal.setNumber1(cal.getNumber1() % num);
	}

	/**
	 * A minus state and set a new state and calculate a number.
	 */
	@Override
	public void performMinus(double num) {
		this.cal.setState(cal.MINUS);
		this.cal.setLastState(cal.MOD);
		this.cal.setNumber1(cal.getNumber1() % num);
	}

	/**
	 * A multiply state and set a new state and calculate a number.
	 */
	@Override
	public void performMultiply(double num) {
		this.cal.setState(cal.MULTIPLY);
		this.cal.setLastState(cal.MOD);
		this.cal.setNumber1(cal.getNumber1() % num);
	}

	/**
	 * A divide state and set a new state and calculate a number.
	 */
	@Override
	public void performDivide(double num) {
		this.cal.setState(cal.DIVIDE);
		this.cal.setLastState(cal.MOD);
		this.cal.setNumber1(cal.getNumber1() % num);
	}

	/**
	 * A result state and set a new state and calculate a number.
	 */
	@Override
	public void performResult(double num) {
		this.cal.setState(cal.RESULT);
		this.cal.setLastState(cal.MOD);
		this.cal.performOperation();
	}

	/**
	 * A mod state and set a new state and calculate a number.
	 */
	@Override
	public void performMod(double num) {
		this.cal.setState(cal.MOD);
		this.cal.setLastState(cal.MOD);
		this.cal.setNumber1(cal.getNumber1() % num);
	}
}
