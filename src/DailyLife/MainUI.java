package DailyLife;


import java.net.URL;
import java.util.Calendar;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Clock.Clock;
import Clock.ClockUI;



import com.toedter.calendar.JCalendar;

/**
 * Main UI for Daily Life Helper.
 * @author Arut Thanomwatana,Narut Poovorakit
 *
 */
public class MainUI extends JFrame{


	private boolean firstTime = false;
	private JFrame frame;
	private JLabel labelPicBG, clock, pigLabel;
	private JPanel panel;
	private JButton reminderBut, calculatorBut, serverBut, exitBut;
	private JCalendar calendar;

	private int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

	private int month = Calendar.getInstance().get(Calendar.MONTH);

	private int year = Calendar.getInstance().get(Calendar.YEAR);

	/**
	 * Launch the application.
	 */
	//	public static void main(String[] args) {
	//		EventQueue.invokeLater(new Runnable() {
	//			public void run() {
	//				try {
	//					MainUI window = new MainUI();
	////					window.frame.setVisible(true);
	//				} catch (Exception e) {
	//					e.printStackTrace();
	//				}
	//			}
	//		});
	//	}

	/**
	 * Create the application.
	 */
	public MainUI() {
		setBounds(100,100,800,400);
		super.setUndecorated(true);
		super.setDefaultCloseOperation(EXIT_ON_CLOSE);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		ClassLoader cls = this.getClass().getClassLoader();
		//URL url = cls.getResource("../images/mainBG.png");
		URL url = cls.getResource("images/mainBG.png");
		//When In Jar File Cannot load.
		ImageIcon imgIcon = new ImageIcon(url);


		//Clock
		clock = new JLabel();
		clock.setBounds(0,0,100,100);
		Clock clock = new Clock();
		ClockUI cheap = new ClockUI(clock);
		cheap.setBounds(580, 30, 160, 100);
		super.add(cheap);

		pigLabel = new JLabel();
		URL urlPig = cls.getResource("images/pigPic.gif");
		ImageIcon iconPig = new ImageIcon(urlPig);
		pigLabel.setIcon(iconPig);
		pigLabel.setBounds(660,280,200,100);

		//Button
		URL url2 = cls.getResource("images/exitBut.png");
		ImageIcon imgIcon2 = new ImageIcon(url2);
		exitBut = new JButton();
		exitBut.setBounds(740, 11, 50, 50);
		exitBut.setContentAreaFilled(false);
		exitBut.setIcon(imgIcon2);
		exitBut.addActionListener(p ->{
			super.setVisible(false);
			System.exit(EXIT_ON_CLOSE);
		});

		calculatorBut = new JButton();  
		calculatorBut.setBounds(228, 246, 226, 55);
		calculatorBut.setBorderPainted(false);
		calculatorBut.setContentAreaFilled(false);
		calculatorBut.addActionListener(p ->{
			Calculator.CalculatorUI ui = new Calculator.CalculatorUI();
			ui.run();
		});

		reminderBut = new JButton();
		reminderBut.setBounds(387, 316, 214, 44);
		reminderBut.setBorderPainted(false);
		reminderBut.setContentAreaFilled(false);
		reminderBut.addActionListener(p ->{
			Reminder.ReminderUI ui = new Reminder.ReminderUI(this.day,this.month,this.year);
			ui.run();
		});

		labelPicBG = new JLabel();
		labelPicBG.setIcon(imgIcon);
		labelPicBG.setBounds(0,11,800,400);

		panel = new JPanel();
		panel.setLayout(null);

		calendar = new JCalendar();
		calendar.setBounds(565, 158, 198, 153);
		calendar.setEnabled(true);
		calendar.getDayChooser().addPropertyChangeListener(p ->{
			this.day = calendar.getDayChooser().getDay();
			if(firstTime){
				Reminder.ReminderUI ui = new Reminder.ReminderUI(this.day,this.month,this.year);
				ui.run();
			}
		});
		calendar.getMonthChooser().addPropertyChangeListener(p ->{
			this.month = calendar.getMonthChooser().getMonth();
			calendar.setEnabled(true);
		});
		calendar.getYearChooser().addPropertyChangeListener(p ->{
			this.year = calendar.getYearChooser().getYear();
			calendar.setEnabled(true);
		});

		panel.add(pigLabel);
		panel.add(calendar);
		panel.add(reminderBut);
		panel.add(calculatorBut);

		serverBut = new JButton();
		serverBut.setBounds(23, 176, 195, 200);
		serverBut.setBorderPainted(false);
		serverBut.setContentAreaFilled(false);
		serverBut.addActionListener(p ->{
			ImageSender.ImageSenderUI ui = new ImageSender.ImageSenderUI("localhost", 5555);
			ui.run();
		});


		panel.add(exitBut);
		panel.add(serverBut);
		panel.add(labelPicBG);

		super.add(panel);
	}

	/**
	 * Run the UI
	 */
	public void run(){
		super.setVisible(true);
		this.firstTime = true;
	}
}
