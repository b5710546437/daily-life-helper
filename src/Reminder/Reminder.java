package Reminder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

/**
 * Reminder is the note for write the message for you to remind them later.
 * @author Arut Thanomwatana,Narut Poovorakit
 *
 */
public class Reminder 
{

	private List<String> history;
	private final String fileName = "reminder.txt";

	/**
	 * Initialize the Reminder
	 * 
	 */
	public Reminder(){
		
	}

	/**
	 * Write the reminder in to the text file when save.
	 * @param write is the String that will be write into the text file
	 */
	public void writeReminder(String write){
		try{
			File checkFile = new File(fileName);
			if(!checkFile.isDirectory())
				checkFile.createNewFile();
			
			BufferedWriter output = new BufferedWriter((new FileWriter(fileName)));
			String [] writeSplit = write.split("-");
			for(String written : writeSplit){
				if(!written.equals("null")&&!written.equals("")){
					output.write(written);
					output.newLine();
				}
			}
			output.close();
		}catch(IOException e){
			System.out.println("Unexpected Error from WriteReminder Method");
		}

	}

	/**
	 * Read the reminder in the text file when open the app.
	 * @return String [] for the reminder
	 */
	public String [] readReminder(){
		File file = new File(fileName);
		int line = 0;
		try {
			Scanner lineCheck = new Scanner(file);
			while(lineCheck.hasNextLine()){
				line++;
				lineCheck.nextLine();
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(line ==0)
			line++;
		String [] remindList = new String[line];
		if(!file.exists()){
			try{
				file.createNewFile();
			}catch(IOException e){
				System.out.println("Unexpected Error on creating file");
			}
		}
		try{
			Scanner scan = new Scanner(file);
			int index = 0;
			while(scan.hasNextLine()){
				String next = scan.nextLine();
				if(next.equals(""))
					continue;

				remindList[index] = next;
				index++;

			}
			scan.close();
		} catch (FileNotFoundException e){
			System.out.println("Unexpected Error from readReminder");
		}

		return remindList;
	}

	/**
	 * Get the Reminder history
	 * @return List that read from the textfile.
	 */
	public List <String> getHistory(){
		history = new ArrayList<String>();
		String [] remindList = readReminder();
		for(String remind : remindList){
			history.add(remind);
		}
		
		return history;
	}

}
