package Reminder;

import java.awt.EventQueue;
import java.awt.Font;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import java.awt.Color;
import java.awt.event.ActionEvent;

/**
 * ReminderUI is the UI for Reminder.
 * @author Arut Thanomwatana,Narut Poovorakit
 *
 */
public class ReminderUI extends JFrame{

	final static int PRESENT = Calendar.getInstance().get(Calendar.YEAR)-543;
	List <String> remindList;
	private JFrame frame;
	JPanel bgPane;
	private JLabel labelPic;
	private JLabel textPic;
	private JTextArea remindArea;
	private JButton saveButton;
	private JComboBox<String> monthComBox, dayComBox, yearComBox;
	private String[] monthList, dayList, yearList;
	ImageIcon img1;
	ImageIcon img2;
	private Reminder reminder;
	
	private int day;
	
	private int month;
	
	private int year;

	/**
	 * Create the application.
	 */
	public ReminderUI(int day,int month,int year) {

		remindList = new ArrayList<String>();
		super.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(0, 0, 800, 600);
		reminder = new Reminder();
		remindList = reminder.getHistory();
		remindList.remove(0);
		
		this.day = day;
		this.month = month;
		this.year = year;
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		final SaveAction saveAction = new SaveAction();

		//Combobox
		dayList = new String[31];
		for( int i = 1 ; i <= 31 ; i++ ) {
			dayList[i-1] = Integer.toString(i);
		}
		dayComBox = new JComboBox<String>(dayList);
		dayComBox.setBounds(75, 100, 100, 20);
		dayComBox.addActionListener(new SetAction());
		dayComBox.setSelectedIndex(this.day-1);


		monthList = new String[]{"January","February","March","April","May",
				"June","July","August","September","October","November","December"};
		monthComBox = new JComboBox<String>(monthList);
		monthComBox.setBounds(190, 100, 120, 20);
		monthComBox.addActionListener(new SetAction());
		monthComBox.setSelectedIndex(this.month);

		yearList = new String[10];
		for( int i = 0 ; i < 10 ; i++ ) {
			yearList[i] = Integer.toString(i + this.year);
		}
		yearComBox = new JComboBox<String>(yearList);
		yearComBox.setBounds(320, 100, 100, 20);
		yearComBox.addActionListener(new SetAction());
		yearComBox.setSelectedIndex(this.year - PRESENT);

		bgPane = new JPanel();
		bgPane.setLayout(null);
		labelPic = new JLabel();
		textPic = new JLabel();
		saveButton = new JButton(saveAction);


		ClassLoader cls = this.getClass().getClassLoader();
		URL url = cls.getResource("images/reminderImage.png");
		img1 = new ImageIcon(url);
		labelPic.setIcon(img1);
		labelPic.setBounds(21,11,219,77);

		URL url2 = cls.getResource("images/reminderText.png");
		img2 = new ImageIcon(url2);
		textPic.setIcon(img2);
		textPic.setBounds(0,0,784,561);

		saveButton.setBounds(600,470,90,30);

		bgPane.add(yearComBox);
		bgPane.add(dayComBox);
		bgPane.add(monthComBox);
		bgPane.add(saveButton);
		initTextArea();	
		bgPane.add(labelPic);
		bgPane.add(textPic);


		super.add(bgPane);


	}

	/**
	 *Init the text area in the reminder. 
	 */
	public void initTextArea(){
		remindArea = new JTextArea();

		remindArea.setForeground(Color.BLACK);
		remindArea.setBackground(Color.LIGHT_GRAY);
		remindArea.setBounds(116, 132, 300, 300);


		bgPane.add(remindArea);


	}

	/**
	 * Run the UI
	 */
	public void run(){
		super.setVisible(true);
	}

	/**
	 * Set the text area in reminder when you go to the day that match the history.
	 * @param remind is the String that will be set
	 */
	public void setTextArea(String remind){
		remindArea.setText(remind);

	}

	class SaveAction extends AbstractAction{
		public SaveAction(){
			super("Save");
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			StringBuffer toWrite = new StringBuffer();
			if(remindArea.getText().equals("")){
				for(int i =0;i<remindList.size();i++){
					String check = remindList.get(i);
					String [] splitCheck = check.split(",");
					if(splitCheck[1].equals(dayComBox.getSelectedItem())&&splitCheck[2].equals(monthComBox.getSelectedItem())&&splitCheck[3].equals(yearComBox.getSelectedItem())){
						remindList.remove(i);
					}
				}
			}
			else{
				if(remindList.size() == 0){
					remindList.add((String.format("%s,%s,%s,%s",remindArea.getText(),dayComBox.getSelectedItem(),monthComBox.getSelectedItem(),yearComBox.getSelectedItem())));
				}
				else{
					boolean willAdd = true;
					boolean duplicateAdd = false;
					String duplicate = "";
					for(int i = 0 ;i<remindList.size();i++){
						String check = remindList.get(i);
						String [] splitCheck = check.split(",");
						if(splitCheck[1].equals(dayComBox.getSelectedItem())&&splitCheck[2].equals(monthComBox.getSelectedItem())&&splitCheck[3].equals(yearComBox.getSelectedItem())){
							if(!(splitCheck[0].equals(remindArea.getText()))){
								duplicateAdd = true;
								duplicate = check;
							}
							willAdd = false;


						}
					}
					if(willAdd)
						remindList.add((String.format("%s,%s,%s,%s",remindArea.getText(),dayComBox.getSelectedItem(),monthComBox.getSelectedItem(),yearComBox.getSelectedItem())));
					if(duplicateAdd){
						remindList.remove(duplicate);
						remindList.add((String.format("%s,%s,%s,%s",remindArea.getText(),dayComBox.getSelectedItem(),monthComBox.getSelectedItem(),yearComBox.getSelectedItem())));
					}
				}
				for(String remind : remindList)
				{
					toWrite.append(String.format("-%s",remind));
				}
			}
			reminder.writeReminder(toWrite.toString());
		}

	}

	class SetAction extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			remindList = reminder.getHistory();
			for(int i =0;i<remindList.size();i++){
				if(remindList.get(i) == null)
					remindList.remove(i);
			}
			for(String remind : remindList){
				if(remind!=null){
					String [] splitCheck = remind.split(",");
					if(splitCheck[1].equals(dayComBox.getSelectedItem())&&splitCheck[2].equals(monthComBox.getSelectedItem())&&splitCheck[3].equals(yearComBox.getSelectedItem())){
						setTextArea(splitCheck[0]);
					}
				}
			}
		}
	}
}
