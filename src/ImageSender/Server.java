package ImageSender;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;

/**
 * Server is the server for Image Sender program
 * @author Arut Thanomwatana,Narut Poovorakit
 *
 */
public class Server extends AbstractServer 
{

	final static int LOGGEDIN = 2;
	final static int WHISPER = 1;
	final static int LOGGEDOUT = 0;
	final static int SENTFILE = 4;
	private static final String SAVE_POSITION = "D:/OOP2/Week2/Project/Daily Life/src/Server/";
	public final static int FILE_SIZE = 6022386;
	private List<ConnectionToClient> clients = new ArrayList<ConnectionToClient>();
	private ServerDownloader downloader;

	private int bytesRead;

	/**
	 * Initialize the server.
	 * @param port is the port for this server
	 */
	public Server(int port) {
		super(port);
		downloader = new ServerDownloader(this);
	}

	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		String message = "";
		File file = null;
		int state = (Integer)client.getInfo("state");
		try{
			if(msg instanceof File){
				file = (File)msg;
			}
			else
				message = (String)msg;
		}catch(ClassCastException e){
			super.sendToAllClients("Unknown Message");
		}

		switch(state){
		case LOGGEDOUT:
			if(message.matches("Login \\w+")){
				String username = message.substring(6).trim();
				client.setInfo("username",username);
				client.setInfo("state", LOGGEDIN);
				super.sendToAllClients(client.getInfo("username")+" connected.");
			}
			else{
				sentToClient(client, "Please login");
			}
			break;
		case LOGGEDIN:
			if(message.equalsIgnoreCase("Logout"))
			{
				client.setInfo("state", LOGGEDOUT);
				super.sendToAllClients(client.getInfo("username")+" disconnected.");
			}
			else if(message.matches("To: \\w+")){
				String username = message.substring(4).trim();	
				for(ConnectionToClient c : clients){
					if(username.equals((String)c.getInfo("username"))){
						client.setInfo("whisper",c.getInfo("username"));
						client.setInfo("state", WHISPER);
					}
				}
			}
			else if(message.matches("Sent: \\w+")){
				String username = message.substring(5).trim();
				for(ConnectionToClient c : clients){
					if(username.equals("all")){
						client.setInfo("sent", "all");
						client.setInfo("state", SENTFILE);
					}
					if(username.equals((String)c.getInfo("username"))){
						client.setInfo("sent", c.getInfo("username"));
						client.setInfo("state", SENTFILE);
					}
				}
			}
			else if(file != null){
//				downloader.setFileToDownload(file);
//				downloader.execute();
				for(ConnectionToClient c : clients){
					if(client.getInfo("username").equals(c.getInfo("username"))){
						continue;
					}
					else

						if((int)c.getInfo("state")==LOGGEDOUT)
							sentToClient(client,c.getInfo("username")+" is not logged in.");
						else{
							sentToClient(client,"Sending File To " + c.getInfo("username"));
							sentToClient(c,"Receiving file from " + client.getInfo("username"));
							sentToClient(c,file);
						}
				}
			}
			else 
			{
				super.sendToAllClients(client.getInfo("username") + " : " + message);
			}
			break;
		case WHISPER:
			for(ConnectionToClient c : clients){
				if(client.getInfo("whisper").equals((String)c.getInfo("username"))){
					if((int)c.getInfo("state")==LOGGEDOUT)
						sentToClient(client,c.getInfo("username")+" is not logged in.");
					else{
						sentToClient(c,message);
					}
				}
			}
			client.setInfo("state", LOGGEDIN);
			break;
		case SENTFILE:
			downloader.setFileToDownload(file);
			downloader.execute();
			for(ConnectionToClient c : clients){
				if(client.getInfo("sent").equals(c.getInfo("username"))){
					if((int)c.getInfo("state")==LOGGEDOUT)
						sentToClient(client,c.getInfo("username")+" is not logged in.");
					else{
						sentToClient(client,"Sending File To " + c.getInfo("username"));
						sentToClient(c,"Receiving file from " + client.getInfo("username"));
						sentToClient(c,file);
					}			
				}
			}
			client.setInfo("state", LOGGEDIN);
			break;
		}


	}

	/**
	 * Recieved the file from the client and send it to another client.
	 * @param message is the File that will be recieved and forward.
	 * @return Amount of byte that read from the file
	 * @throws IOException if the file cannot be read
	 */
	public Integer recievedFile(File message) throws IOException
	{
		bytesRead = 0;
		int current;
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(message);
			fos = new FileOutputStream(SAVE_POSITION+message.getName());
			bos = new BufferedOutputStream(fos);
			byte [] mybytearray  = new byte [FILE_SIZE];
			bytesRead = fis.read(mybytearray,0,mybytearray.length);
			current = bytesRead;
			do {
				bytesRead = fis.read(mybytearray, current, (mybytearray.length-current));
				if(bytesRead >= 0) current += bytesRead;
			} while(bytesRead > -1);

			bos.write(mybytearray, 0 , current);
			bos.flush(); 	
		} finally {
			if (fos != null) fos.close();
			if (bos != null) bos.close();
			if (fis != null) fis.close();
		}
		return bytesRead;
	}

	/**
	 * Sent the File to client.
	 * @param client is the client that will be sent to
	 * @param msg is the file that will be sent to
	 */
	private void sentToClient(ConnectionToClient client, File msg) {
		try {
			client.sendToClient(msg);
		} catch (IOException e) {

		}

	}

	/**
	 * Sent the message to client.
	 * @param client is the client that will be sent to
	 * @param msg is the message that will be sent to
	 */
	private void sentToClient(ConnectionToClient client,String msg){
		try {
			client.sendToClient(client.getInfo("username") + " --> "+(String)msg);
		} catch (IOException e) {

		}
	}

	/**
	 * Add the client to the clients.
	 * @param client is the client that just connect
	 */
	protected void clientConnected(ConnectionToClient client)
	{
		client.setInfo("state", LOGGEDOUT);
		clients.add(client);

	}

	/**
	 * remove the client from the clients.
	 * @param client is the client that just disconnect
	 */
	protected synchronized void clientDisconnected(ConnectionToClient client){
		super.sendToAllClients("Disconnected :  "+client.getInfo("username"));
		clients.remove(client);
	}

	/**
	 * Main use for start the server.
	 * @param args is not used
	 */
	public static void main(String [] args){
		Scanner scan = new Scanner(System.in);
		Server server = new Server(5555);
		try{
			server.listen();
			while(server.isListening()){

			}
			server.close();

		}catch(IOException e){
			e.printStackTrace();
		}


	}

}
