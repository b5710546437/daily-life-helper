package ImageSender;


import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.List;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.plaf.FileChooserUI;

/**
 * ImageSenderUI is the UI for ImageSender
 * @author Arut Thanomwatana,Narut Poovorakit
 *
 */
public class ImageSenderUI
extends Frame
{
	private static final int DEFAULT_PORT = 5555;
	private static final String DEFAULT_HOST = "localhost";
	private Button closeButton;
	private Button openButton;
	private Button sendButton;
	private Button quitButton;
	private Button browseButton;
	private Button saveFileButton;
	private TextField portField;
	private TextField hostField;
	private TextField messageField;
	private JFileChooser browse;
	private JFileChooser saveAt;
	private Panel southPanel;
	private Label portLabel;
	private Label hostLabel;
	private Label nameLabel;
	private Label name;
	private Font font = new Font("Angsana New", 0, 16);
	private List liste;
	private Client client;
	private int port = 5555;
	private String host = "localhost";

	/**
	 * Initialize the UI.
	 * @param host is the host that will be set as a defualt
	 * @param port is the port that will be set as a defualt
	 */
	public ImageSenderUI(String host, int port)
	{
		super("Simple Client");
		if ((host != null) && (host.length() > 0)) {
			this.host = host;
		}
		if (port > 0) {
			this.port = port;
		}
		this.liste = new List();

		initComponents();

		this.client = new Client(host, port, this.liste);
		this.portField.setText(String.valueOf(port));
		this.hostField.setText(host);

		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				ImageSenderUI.this.hide();
			}
		});
		this.quitButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				ImageSenderUI.this.hide();
			}
		});
		this.closeButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				ImageSenderUI.this.close();
			}
		});
		this.openButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				ImageSenderUI.this.open();
			}
		});
		ActionListener messageActionListener = new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				ImageSenderUI.this.send();
			}
		};
		ActionListener browseActionListener = new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				ImageSenderUI.this.browse();
			}
		};
		ActionListener saveAtActionListener = new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				ImageSenderUI.this.saveAt();
			}
		};
		this.sendButton.addActionListener(messageActionListener);
		this.messageField.addActionListener(messageActionListener);
		this.browseButton.addActionListener(browseActionListener);
		this.saveFileButton.addActionListener(saveAtActionListener);
	}

	/**
	 * Init the component for UI.
	 */
	private void initComponents()
	{
		int gap = 5;

		Font messageFont = new Font("DialogInput", 0, 18);
		this.closeButton = new Button("Close");
		this.openButton = new Button("Open");
		this.sendButton = new Button("Send");
		this.quitButton = new Button("Quit");
		this.browseButton = new Button("Browse");
		this.saveFileButton = new Button("Save At");
		this.portField = new TextField(8);
		this.portField.setText(Integer.toString(this.port));
		this.hostField = new TextField(24);
		this.hostField.setText(this.host);
		this.messageField = new TextField();
		this.name = new Label("");
		this.portLabel = new Label("Port:", 2);
		this.hostLabel = new Label("Host:", 2);
		this.nameLabel = new Label("Name : ");


		Panel hostPanel = new Panel();
		hostPanel.setLayout(new FlowLayout(0, 5, 5));
		hostPanel.add(this.hostLabel);
		hostPanel.add(this.hostField);
		hostPanel.add(this.portLabel);
		hostPanel.add(this.portField);
		hostPanel.add(this.openButton);
		hostPanel.add(this.closeButton);

		Panel msgPanel = new Panel();
		msgPanel.setLayout(new GridLayout(1, 1));
		msgPanel.add(this.messageField);
		this.messageField.setFont(messageFont);

		Panel buttonPanel = new Panel();
		buttonPanel.setLayout(new GridLayout(1, 4, 5, 5));
		buttonPanel.add(this.sendButton);
		buttonPanel.add(this.quitButton);

		Panel southPanel = new Panel();
		southPanel.setLayout(new GridLayout(2, 1, 5, 5));
		southPanel.add(msgPanel);
		southPanel.add(buttonPanel);
		southPanel.add(browseButton);
		southPanel.add(nameLabel);
		southPanel.add(saveFileButton);
		southPanel.add(name);

		Component[] arrayOfComponent;
		int j = (arrayOfComponent = hostPanel.getComponents()).length;
		for (int i = 0; i < j; i++)
		{
			Component c = arrayOfComponent[i];c.setFont(this.font);
		}
		j = (arrayOfComponent = buttonPanel.getComponents()).length;
		for (int i = 0; i < j; i++)
		{
			Component c = arrayOfComponent[i];c.setFont(this.font);
		}
		this.liste.setFont(messageFont);

		setLayout(new BorderLayout(5, 5));

		add("North", hostPanel);

		add("Center", this.liste);

		add("South", southPanel);
		pack();
	
	}

	/**
	 * Read the field from UI.
	 */
	private void readFields()
	{
		int p = Integer.parseInt(this.portField.getText());
		this.client.setPort(p);
		this.client.setHost(this.hostField.getText());
	}

	/**
	 * cut the connection from server.
	 */
	public void close()
	{
		try
		{
			readFields();
			this.client.closeConnection();
		}
		catch (Exception ex)
		{

			ex.printStackTrace();
			this.liste.add(ex.toString());
			this.liste.makeVisible(this.liste.getItemCount() - 1);
			this.liste.setBackground(Color.red);
		}
	}

	/**
	 * open the connection to server.
	 */
	public void open()
	{
		try
		{
			readFields();
			this.client.openConnection();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			this.liste.add(ex.toString());
			this.liste.makeVisible(this.liste.getItemCount() - 1);
			this.liste.setBackground(Color.red);
		}
	}

	/**
	 * send the message to the server.
	 */
	public void send()
	{
		try
		{
			if(this.messageField.getText().matches("Login \\w+")){

				String username = this.messageField.getText().substring(6);
				this.name.setText(username);
			}
			File sentFile = new File(this.messageField.getText());
			if(sentFile.canRead()){
				this.client.sendToServer(sentFile);
			}
			else
				this.client.sendToServer(this.messageField.getText());
			this.messageField.setText("");

		}catch (Exception ex)
		{

			ex.printStackTrace();
			this.liste.add(ex.toString());
			this.liste.makeVisible(this.liste.getItemCount() - 1);
			this.liste.setBackground(Color.yellow);
		}

	}

	/**
	 * Exit the UI
	 */
	public void quit()
	{
		System.exit(0);
	}


	/**
	 * Browse the file for sending.
	 */
	public void browse(){
		browse = new JFileChooser();
		browse.showOpenDialog(southPanel);
		try{
			messageField.setText(browse.getSelectedFile().getPath());
		}catch(NullPointerException e){
			messageField.setText("");
		}
	}

	/**
	 * Set the path for the file.
	 */
	public void saveAt(){
		saveAt = new JFileChooser();
		saveAt.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		saveAt.setAcceptAllFileFilterUsed(false);
		saveAt.showOpenDialog(southPanel);
		String path = saveAt.getSelectedFile().toString();
		client.setPath(path);

	}
	
	public void run(){
		setVisible(true);
	}



}

