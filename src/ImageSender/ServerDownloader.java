package ImageSender;

import java.io.File;

import javax.swing.SwingWorker;
/**
 * File Downloader for server.
 * @author Arut Thanomwatana,Narut Poovorakit
 *
 */

public class ServerDownloader extends SwingWorker<Integer,Integer> implements Runnable {

	private Server server;
	
	private File fileToDownload;

	/**
	 * Init the Server downloader.
	 * @param server is the user of this downloader
	 */
	public ServerDownloader(Server server){
		this.server = server;
	}
	/**
	 * Set the file for downloader to download.
	 * @param file is the file that will be set for download
	 */
	public void setFileToDownload(File file)
	{
		this.fileToDownload = file;
	}
	@Override
	protected Integer doInBackground() throws Exception {
		return server.recievedFile(fileToDownload);
	}
}
