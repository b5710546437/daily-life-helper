package ImageSender;

import java.awt.Color;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.awt.List;
import java.util.Scanner;

import com.lloseng.ocsf.client.AbstractClient;


/**
 * Client is the class that will handle as a client in the server.
 * @author Arut Thanomwatana,Narut Poovorakit
 *
 */
public class Client extends AbstractClient
{
	final static String server = "127.0.0.1";
	final static int port = 5555;
	public String FILE_TO_RECEIVED = "D:/OOP2/Week2/Project/Daily life/src/Client/";
	public final static int FILE_SIZE = 6022386;

	public ClientDownloader downloader;
	private int bytesRead;
	private List liste;

	/**
	 * Initialize Client
	 * @param liste is the list of String that will show in the ui
	 */
	public Client(List liste)
	{
		super("localhost", 12345);
		this.liste = liste;
		downloader = new ClientDownloader(this);
	}

	/**
	 * Initialize Client.
	 * @param liste is the list of String that will show in the ui
	 * @port port is the port that used to connect to server
	 */
	public Client(int port, List liste)
	{
		super("localhost", port);
		this.liste = liste;
		downloader = new ClientDownloader(this);
	}

	/**
	 * Initialize Client.
	 * @param liste is the list of String that will show in the ui
	 * @port port is the port that used to connect to server
	 * @host host is the ip of the host server
	 */
	public Client(String host, int port, List liste)
	{
		super(host, port);
		this.liste = liste;
		downloader = new ClientDownloader(this);
	}

	@Override
	protected void handleMessageFromServer(Object msg) 
	{
		if(msg == null){
			return;
		}
		if(msg instanceof String){
			this.liste.add(msg.toString());
			this.liste.makeVisible(this.liste.getItemCount() - 1);
		}
		else if(msg instanceof File){
			File message = (File)msg;
			downloader.setFileToDownload(message);
			downloader.execute();

		}

	}
	
	/**
	 * get the list of the String.
	 * @return list of the String
	 */
	public List getList(){
		return this.liste;
	}

	/**
	 * Download use to load the file from other client (or server).
	 * @param message is the file that will be load
	 * @return bytesRead is the amount of byte that were already load
	 * @throws IOException when File can't be read
	 */
	public Integer download(File message) throws IOException{
		bytesRead = 0;
		int current;
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(message);
			fos = new FileOutputStream(FILE_TO_RECEIVED+"/"+message.getName());
			bos = new BufferedOutputStream(fos);
			byte [] mybytearray  = new byte [FILE_SIZE];
			bytesRead = fis.read(mybytearray,0,mybytearray.length);
			current = bytesRead;
			do {
				bytesRead = fis.read(mybytearray, current, (mybytearray.length-current));
				if(bytesRead >= 0) current += bytesRead;
			} while(bytesRead > -1);

			bos.write(mybytearray, 0 , current);
			bos.flush(); 	
		} finally {
			if (fos != null) fos.close();
			if (bos != null) bos.close();
			if (fis != null) fis.close();
		}
		return bytesRead;
	}

	/**
	 * Add the String to the UI when connection is closed.
	 */
	protected void connectionClosed()
	{
		this.liste.add("**Connection closed**");
		this.liste.makeVisible(this.liste.getItemCount() - 1);
		this.liste.setBackground(Color.pink);
	}

	/**
	 * Add the String to the UI when an exception is occur.
	 */
	protected void connectionException(Exception exception)
	{
		this.liste.add("**Connection exception: " + exception);
		this.liste.makeVisible(this.liste.getItemCount() - 1);
		this.liste.setBackground(Color.red);
	}

	/**
	 * Add the String to the UI when a Connection is established.
	 */
	protected void connectionEstablished()
	{
		this.liste.add("--Connection established");
		this.liste.makeVisible(this.liste.getItemCount() - 1);
		this.liste.setBackground(Color.green);
	}
	
	/**
	 * Set the paht for save the file.
	 * @param path is the path that will be saved
	 */
	public void setPath(String path){
		this.FILE_TO_RECEIVED = path;
	}




}
