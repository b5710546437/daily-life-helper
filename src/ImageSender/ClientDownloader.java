package ImageSender;

import java.io.File;

import javax.swing.SwingWorker;

/**
 * ClientDownloader is a Downloader for client.
 * @author Arut Thanomwatana,Narut Poovorakit
 *
 */

public class ClientDownloader extends SwingWorker<Integer,Integer> implements Runnable {

	private Client client;
	
	private File fileToDownload;
	
	/**
	 * Initialize Client Downloader.
	 * @param client is the user if this downloader
	 */
	public ClientDownloader(Client client){
		this.client = client;
		
	}
	
	/**
	 * Set File to download for downloader.
	 * @param file is the file that will be set for download
	 */
	public void setFileToDownload(File file){
		
		this.fileToDownload = file;
	}

	@Override
	protected Integer doInBackground() throws Exception {
		return client.download(fileToDownload);
	}
	
	@Override
	protected void done(){
		client.getList().add("DONE!!!");
		client.getList().makeVisible(client.getList().getItemCount()-1);
		super.done();
	}
}
